const tls = require('tls')
tls.DEFAULT_ECDH_CURVE = 'auto'
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'krupkin-client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'layer&apos;s frontend with ssr under vuejs&apos;s nuxt' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', type: 'image/png', sizes:"57x57", href: '/apple-icon-57x57.png' },
      { rel: 'icon', type: 'image/png', sizes:"32x32", href: "/favicon-32x32.png" },
      // { rel: 'stylesheet', href: '/css/fonts.css' },
      // { rel: 'stylesheet', href: '/css/common.css'},
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: '~/components/loading.vue',
  /*
  ** Build configuration
  */
  build: {
    vendor: ['moment', 'axios'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  plugins: [
    '~/plugins/bootstrap',
    '~/plugins/vmask',
    { src: '~/plugins/moment', ssr: false},
    { src: '~/plugins/yametrik.js', ssr: false },
    { src: '~/plugins/fbshare.js', ssr: false },
    { src: '~/plugins/vkshare.js', ssr: false },
  ],

  css: [
    'bootstrap/dist/css/bootstrap.css',
    'bootstrap-vue/dist/bootstrap-vue.css',
    '~/assets/css/fonts.css',
    '~/assets/css/animation.css',
    '~/assets/css/common.css'
  ]
}
