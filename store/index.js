import { baseUrl, API } from './api'

export const state = () => ({
  sections: []
})

export const actions = {
  getSections ({commit}) {
    API.get('articles/sections/')
    .then((resp) => {
      commit('setSections', resp);
      return resp
    })
  }
}

export const mutations = {
  setSections (state, sections) {
    state.sections = sections
  }
}
