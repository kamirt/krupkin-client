import Vue from 'vue'
import axios from 'axios'

export const baseUrl = "https://okrupkin.com"
export const API = {
    get(url, request) {
        return axios.get(baseUrl + "/api/" + url, request)
            .then((response) => {return response.data})
            .catch((error) => Promise.reject(error));
    },
    post(url, data) {
      return axios.post(baseUrl + "/api/" + url, data)
          .then((response) => {return response.data})
          .catch((error) => Promise.reject(error));
    }
}
